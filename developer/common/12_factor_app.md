# 12 factor app

1. Codebase

   One codebase tracked in revision control, many deploys

   ![One codebase maps to many deploys](https://12factor.net/images/codebase-deploys.png)

2. Dependencies

   Must commit `yarn.lock`, `package-lock.json`,` Gemfile.lock` for explicit versioning of depencencies

3. Config

   Config is the one that does not vary between environments

   Must not keep configuration as constant in source code (except for `.env.example` file as a form of documentation for configuration)

4. Backing services

   A *backing service* is any service the app consumes over the network as part of its normal operation. Examples include datastores (such as [MySQL](http://dev.mysql.com/) or [CouchDB](http://couchdb.apache.org/)), messaging/queueing systems (such as [RabbitMQ](http://www.rabbitmq.com/) or [Beanstalkd](https://beanstalkd.github.io/)), SMTP services for outbound email (such as [Postfix](http://www.postfix.org/)), and caching systems (such as [Memcached](http://memcached.org/)).

   **The code for a twelve-factor app makes no distinction between local and third party services.** To the app, both are attached resources, accessed via a URL or other locator/credentials stored in the Config above. A [deploy](https://12factor.net/codebase) of the twelve-factor app should be able to swap out a local MySQL database with one managed by a third party (such as [Amazon RDS](http://aws.amazon.com/rds/)) without any changes to the app’s code. Likewise, a local SMTP server could be swapped with a third-party SMTP service (such as Postmark) without code changes. In both cases, only the resource handle in the config needs to change.

5. Build, release, run

   Strictly separate build and run stages

   ![Code becomes a build, which is combined with config to create a release.](https://12factor.net/images/release.png)

6. Processes

   **Twelve-factor processes are stateless and [share-nothing](http://en.wikipedia.org/wiki/Shared_nothing_architecture).** Any data that needs to persist must be stored in a stateful [backing service](https://12factor.net/backing-services) above, typically a database or S3 storage

7. Port binding

   Export services via port binding

   **The twelve-factor app is completely self-contained** and does not rely on runtime injection of a webserver into the execution environment to create a web-facing service. The web app **exports HTTP as a service by binding to a port**, and listening to requests coming in on that port.

8. Concurrency

   Scale out via the process model

   Application must also be able to span multiple processes running on multiple physical machines

   Twelve-factor app processes [should never daemonize](http://dustin.github.com/2010/02/28/running-processes.html) or write PID files

9. Disposability

   Maximize robustness with fast startup and graceful shutdown

10. Dev/prod parity

    Keep development, staging, and production as similar as possible

    **The twelve-factor developer resists the urge to use different backing services between development and production**. Use Docker to keep parity

11. Logs

    **A twelve-factor app never concerns itself with routing or storage of its output stream.** It should not attempt to write to or manage logfiles. Instead, each running process writes its event stream, unbuffered, to `stdout`.

12. Admin processes

    Run admin/management tasks as one-off processes

## References

https://12factor.net
