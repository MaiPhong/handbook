# Git conventions

## Workflow

We use GitLab/GitHub flow. It's a simplified workflow primarily used for web development and continuous deployment. Developers create feature branches from `main`, and after the changes are complete, they are merged back into the `main` branch, which is then immediately deployed.

```mermaid
graph TD
    subgraph Feature branches in GitHub Flow
    A[main branch] ===>B[main branch]
    D[nav branch] --> |add navigation| B
    B ===> C[main branch]
    E[feature-branch] --> |add feature| C
    C ==> F[main branch]
    end
```



```mermaid
graph LR
	subgraph branching model	
	A[main] --> B[main]
	B --> C[main]
	C --> D[main]
	
	A1[work/fix-race-condition] --> |merge request<br>with rebase| B
  B1[work/fix-frozen-login] --> |merge request<br>with rebase| C
  C1[work/fix-frozen-login] --> |merge request<br>with rebase| D
	
	B --> |deploy to<br>test| G[env/test]


	G --> I[env/test]


	C --> |deploy to<br>test| I
	B --> |deploy to <br>production| K[env/prod]

	end
```

### Branching model

- main
- env/test
- env/staging
- env/production
- feat/* or fix/*  or work/*

## Commit

- Each commit must be isolated and a complete change (eg: implement 3 feature, 2 fix should have 5 commits)
- Commit often and push frequently
- Commit message template ??
  - A commit message should reflect your intention, not just the contents of the commit. You can see the changes in a commit, so the commit message should explain why you made those changes:

    ```shell
    # This commit message doesn't give enough information
    git commit -m 'Improve XML generation'
    
    # These commit messages clearly state the intent of the commit
    git commit -m 'Properly escape special characters in XML generation'
    ```

## Rebase policy

Having lots of merge commits can make your repository history messy. We want to avoid that **at all cost**; therefore,  Every merge **must**  be fast-forward (no merge commit), we can do that by rebasing our branch to `main` branch before merge

- Should avoid rebasing commits you have pushed to a remote server if you have other active contributors in the same branch.
- If a merge involves many commits, it may seem more difficult to undo. You might consider solving this by squashing all the changes into one commit just before merging by using the GitLab Squash-and-Merge feature


## Merge/Pull requests

- Title template??
- Description template??
- **Must** delete remote feature branches after merged
- If you open the merge request but it's not ready, mark it as [DRAFT]

## References
- https://docs.gitlab.com/ee/topics/gitlab_flow.html
- https://www.atlassian.com/blog/git/git-team-workflows-merge-or-rebase