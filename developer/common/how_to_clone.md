# How To Clone

Clone with ghorg to preserve gitlab group structure like following example

```
# tree ~/ghorg -d -L 2
ghorg
├── myschool2
│   ├── bl*
├── tripathvn
│   ├── Li*
│   ├── Mo*
│   ├── ag*
│   ├── bf*
│   ├── bl*
│   ├── co*
└── ycommvn
    └── we*

```

## Install

https://github.com/gabrie30/ghorg

```shell
brew install ghorg
```

## Config

Create [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens?name=ghorg&scopes=read_api) with `Expiration date` **removed** and add to `~/.config/ghorg/conf.yaml`

```yaml
# nano ~/.config/ghorg/conf.yaml
GHORG_GITLAB_TOKEN: XXXX
GHORG_PRESERVE_DIRECTORY_STRUCTURE: true # MUST CHANGE
GHORG_SCM_TYPE: gitlab # MUST CHANGE
GHORG_CLONE_PROTOCOL: ssh # Optional
GHORG_SKIP_ARCHIVED: true # Optional
```

## Clone

### By Sub Group

```shell
ghorg clone tripathvn/lola
ghorg clone tripathvn/gbye/readme
ghorg clone tripathvn/moykomiks
ghorg clone ycomm_tool/webtoon/proto/auto-translator
ghorg clone ycommvn/handbook
```

### By Root Group

```shell
ghorg clone tripathvn # all
ghorg clone ycommvn # all
ghorg clone ycomm1 # Ms. Lee Platform Repo
ghorg clone ycomm_tool # Ms. Lee Tool repo
ghorg clone myschool2
```

