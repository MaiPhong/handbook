# Squad team

## PM

- Define and ensure schedule of deliverables
- Define milestones and / or epics
- Define user stories
- Define acceptance criteria for user stories
- Manage communication with external stakeholders including not only the Client but also all other third parties

## UI/UX

- Deliver UX and UI according to the task’s requirements
- Keep organized and neat the source files for the UX/UI. All source files must be shared and accessible by the whole team
- Perform market, competitor and user research to find inspiration and solve issues
- Being analytical about the task at hand. Tasks that do not make sense or need more clarifications must be flagged and brought up to the Team Lead and / or Product Manager.
- Understand the whole purpose of the application being developed
  -Participate meaningfully in design review sessions

## QC

- Perform quality controls of the work delivered by developers. QC is performed on the staging / beta environment once the code has been approved by Team Leads and deployed

## Developer

- Tasks that do not make sense or need more clarifications must be flagged and brought up to the Team Lead and / or Product Manager.
- Understand the whole design of the application being developed
- Participate meaningfully in technical decisions
- Perform research to solve issues


## Squad lead (ephemeral role with 6 month rotation)

- Technical decision maker
- Collaborate on the Sprints’ Backlog with PM (initial estimation, add chore tasks)
- Assigning a task to a developer
- Monitor the progress made by the developers daily
- Change backlog items ordering
- Review code
- Merge pull requests
- Release manager
- First respondent to issues

## Engineering lead

- Accountability for technical of project
- Will conduct informal progress check-ins with their group members every 8 weeks
