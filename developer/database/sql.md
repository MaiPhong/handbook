# SQL

## Naming convention

Use following rules while design DB Schema

1. Use lowercase letters for table and column names: This helps to avoid confusion with keywords in SQL queries.
1. Use underscores to separate words in table and column names: This improves readability and makes it easier to understand the purpose of each table and column.
1. Use singular nouns for table names: Table names should represent a single entity, so using singular nouns is more appropriate than plural nouns.
1. Use descriptive names for tables and columns: Tables and columns should have names that clearly convey their purpose and meaning.
1. Use abbreviations sparingly: While abbreviations can save space, they can also make your database schema harder to understand. Use abbreviations only when they are widely recognized and do not cause confusion.
1. Use primary key column names in the format "<table_name>_id": This makes it clear which table the primary key belongs to and makes it easier to establish relationships between tables.
1. Use foreign key column names in the format "<referenced_table_name>_id": This makes it clear which table the foreign key references and makes it easier to establish relationships between tables.

Some additional rules for junction table

1. Use a meaningful name: Junction tables are used to represent many-to-many relationships between tables, so it's important to give them a name that reflects this relationship. A common convention is to use the names of the two related tables, separated by an underscore.
1. Use singular nouns: Like other table names, junction table names should use singular nouns, since they represent a single entity.
1. Include primary keys for both related tables: Junction tables typically have two foreign keys, each of which references the primary key of one of the related tables. The primary key of the junction table should consist of both foreign keys, forming a composite key that uniquely identifies each relationship.
1. Avoid adding additional columns: Junction tables should only include the two foreign keys, and should not include any additional columns. If you need to store additional information about the relationship, you can create a separate table with its own foreign key that references the junction table.
1. Follow the same naming conventions and rules for foreign keys in the junction table as you would for any other foreign key.
